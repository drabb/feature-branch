import os, osproc, re, terminal

## todo
## - add help text if no flags
## - allow user to choose from existing branch, typeahead?
##   - git branch | grep -v -e 'master' -e 'prod' -e 'staging'
## - makefile with build, release `nim c -d:release main`
## - progress bar
##   - https://nim-lang.org/docs/terminal.html#progress-bar


proc echoHelpText() =
  echo """
feature_branch
Easily create feature branches using ClickUp branch conventions

USAGE:
    feature_branch <clickup_card_id> <snake_case_name>
    feature_branch # creates branch interactively

OPTIONS:
    -c, --choose    Present TUI branch chooser
    -h, --help      Display this message

EXAMPLE:
    feature_branch CU-34abce3a add_boosters
  """

proc styledReadLine(style: ForegroundColor, prompt: string): string =
  stdout.styledWriteLine(fgGreen, prompt)
  cursorUp 1
  cursorForward len(prompt)
  readLine(stdin)

proc presentBranchChooser() =
  echo "The -c or --choose flag was provided."

proc createBranch(name: string, desc: string) =
  let branchName = name & "_" & desc
  stdout.styledWriteLine(fgBlue, "\nCreating new branch '", fgYellow, branchName, fgBlue, "' from master...")
  let gitComCmd = "git com"
  let gitCheckoutCmd = "git checkout -b CU-" & branchName
  stdout.styledWriteLine(fgBlue, "\nChecking out master")
  var (output, exitCode) = execCmdEx(gitComCmd)
  echo output
  stdout.styledWriteLine(fgBlue, "Creating new feature branch")
  (output, exitCode) = execCmdEx(gitCheckoutCmd)
  echo output

proc main() =
  var branchChooser: bool = false
  var needsHelp: bool = false
  var branchName: string
  var desc: string

  if paramCount() == 2:
    branchName = paramStr(1)
    desc = paramStr(2)
  else:
    for arg in commandLineParams():
      if arg == "-c" or arg == "--choose":
        branchChooser = true
        break
      if arg == "-h" or arg == "--help":
        needsHelp = true
        break

  if needsHelp:
    echoHelpText()
  elif branchChooser:
    presentBranchChooser()
  else:
    if branchName.len == 0:
      branchName = styledReadLine(fgGreen, "ClickUP card ID: ")
      desc = styledReadLine(fgGreen, "Snake case feature name: ")
    branchName = replace(branchName, re"^CU-", "")  # drop "CU-" prefix when matched
    createBranch(branchName, desc)

when isMainModule:
  main()
