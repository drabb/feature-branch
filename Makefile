.PHONY: run
run:
	nim c -r main.nim

.PHONY: build
build:
	nim c main.nim

.PHONY: build-release
build-release:
	nim c -d:release main

